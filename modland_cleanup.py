#!/usr/bin/env python3
import argparse
import os
from collections import Counter
from typing import List


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('path', type=str, help='path to modland filelist')
    return parser.parse_args()


def paths_cleanup(path: str) -> List[str]:
    with open(path, 'rt') as f:
        while True:
            line = f.readline()
            if not line:
                break
            name = os.path.splitext(line.strip())
            if not name[1]:
                char_count = Counter(name[0])
                if name[0] == './' or char_count['/'] < 2:
                    continue
                print(name[0][2:])


if __name__ == '__main__':
    args = parse_args()
    paths_cleanup(args.path)

