#!/usr/bin/env python3
import argparse


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('path', type=str, help='modland artists base path')
    parser.add_argument('name', type=str, help='artist name')
    return parser.parse_args()


def search(path, name):
    with open(path, 'rt') as f:
        while True:
            line = f.readline()
            if not line:
                break
            if name in line.lower():
                print(line.strip())


if __name__ == '__main__':
    args = parse_args()
    search(args.path, args.name.lower())

